// J'importe le package express
const express = require('express')
const db = require('better-sqlite3')('./database/database.db', { verbose: console.log });

// J'execute la fonction express()
const app = express()


const port = 3000

app.get('/', (req, res) => {
    const data = {
        name: "James",
        rates: 17
    }
    res.send(data)
})

var routes = {
    activities: "activities"
}

app.get(`/${routes.activities}`, (req, res) => {
    const id = req.query.id
    const stmt = db.prepare(`SELECT * FROM activities WHERE id = ?`).all(id);
    res.send(stmt)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})